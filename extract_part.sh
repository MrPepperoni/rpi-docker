#! /bin/bash
if [ "$#" -ne 2 ] || ! [ -f "$1" ]; then
  echo "Usage: $0 input output" >&2
  exit 1
fi
fdisk_l=$(fdisk -lu $1)
bs=$(echo "$fdisk_l" | awk '/Sector size/ { print $4; }')
st=$(echo "$fdisk_l" | awk '/Linux/ { print $2; }')
ct=$(echo "$fdisk_l" | awk '/Linux/ { print $4; }')
dd if=$1 of=$2 bs=$bs skip=$st count=$ct

