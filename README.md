# info

the dockerfile creates an ubuntu (eoan) image, extracts sysroot from latest raspbian, installs raspberry pi dev tools, all under /opt/rpi in the container.
you can find a cmake cross-compilation toolchain at /opt/rpi/rpi-toolchain.cmake

# usage

`docker build https://gitlab.com/MrPepperoni/rpi-docker`

