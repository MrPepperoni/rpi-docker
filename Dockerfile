FROM ubuntu:eoan
RUN apt-get update && \
	apt-get install -y openssh-server git python-pip cmake wget unzip fdisk ocfs2-tools && \
	mkdir /var/run/sshd && \
	echo 'root:screencast' | chpasswd && \
	sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config && \
	sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

RUN mkdir -p /opt/rpi/raw /opt/rpi/sysroot
COPY extract_part.sh /opt/rpi/raw/extract_part.sh
COPY extract_fs.sh /opt/rpi/raw/extract_fs.sh
COPY rpi-toolchain.cmake /opt/rpi/rpi-toolchain.cmake

RUN wget https://downloads.raspberrypi.org/raspbian_full_latest -O /opt/rpi/raw/rpi_image.zip && \
	unzip /opt/rpi/raw/rpi_image.zip -d /opt/rpi/raw/ && \
	rm /opt/rpi/raw/rpi_image.zip && \
	sh /opt/rpi/raw/extract_part.sh /opt/rpi/raw/*.img /opt/rpi/raw/linux.ext4 && \
	rm /opt/rpi/raw/*.img && \
	sh /opt/rpi/raw/extract_fs.sh /opt/rpi/raw/linux.ext4 /opt/rpi/sysroot && \
	rm /opt/rpi/raw/linux.ext4 && \
	git clone https://github.com/raspberrypi/tools /opt/rpi/tools

EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]

