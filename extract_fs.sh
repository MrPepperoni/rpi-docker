#! /bin/bash

if [ "$#" -ne 2 ] || ! [ -f "$1" ]; then
  echo "Usage: $0 ext4image output_dir" >&2
  exit 1
fi

ff=$(mktemp)
echo "rdump / $2" >> $ff
debugfs -f $ff $1
rm $ff
